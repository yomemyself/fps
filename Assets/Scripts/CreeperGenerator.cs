﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreeperGenerator : MonoBehaviour {

	public GameObject creeper;
	public Transform[] points;
	public float timeToSpawn;

	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);

			GameObject icreeper = Instantiate (creeper);
			CreeperBehaviour cb = icreeper.GetComponent<CreeperBehaviour> ();
			cb.pathNodes = points;
		}
	}
}
